c:xampp/mysql/bin
cmd:mysql -uroot

1. Membuat Database :
create database myshop;

2. Membuat Table :
Kategori
create table kategori(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

Items
 create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) references kategori(id)
    -> );

Users
create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. Memasukan Data pada Table :
Users
INSERT INTO users(id, name, email, password)
    -> VALUES (1, "John Doe", "john@doe.com", "john123"), (2, "Jane Doe", "jane@doe.com", "jenita123");

Kategori
INSERT INTO kategori(id, name)
    -> VALUES (1, "gadget"), (2, "cloth"), (3, "men"), (4, "women"), (5, "branded");

Items
INSERT INTO items(id, name, description, price, stock, category_id)
    -> VALUES (1, "Sumsang b50", "hape keren dari merek sumsang",  4000000, 100, 1), (2, "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), (3, "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data dari Database :
Mengambil data users
select id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

Mengambil data items
//price > 1.000.000
select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+

//kata kunci uniklo
select * from items where name like 'uniklo%'
    -> ;
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | Uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+

Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items. stock, items.category_id, kategori.name from items inner join kategori on items.category_id = kategori.id;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+

5. Mengubah Data dari Database :
update items set price = 2500000 where id = 1;
