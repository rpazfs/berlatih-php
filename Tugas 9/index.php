
<?php
    //Release 0
    require('animal.php');
    require('ape.php');
    require('frog.php');

    echo "<h3>Sheep</h3>";

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>"; 
    echo "Legs : $sheep->legs <br>"; 
    echo "Cold Blooded : $sheep->cold_blooded <br>"; 

    //Release 1 Ape
    echo "<h3>Ape</h3>";

    $ape = new ape("Kera");

    echo "Name : $ape->name <br>"; 
    echo "Legs : $ape->legs <br>"; 
    echo "Cold Blooded : $ape->cold_blooded <br>"; 
    echo $ape->yell();

    //Release 1 Frog
    echo "<h3>Frog</h3>";

    $kodok = new frog("Katak");

    echo "Name : $kodok->name <br>"; 
    echo "Legs : $kodok->legs <br>"; 
    echo "Cold Blooded : $kodok->cold_blooded <br>"; 
    echo $kodok->yell();
?>