@extends('master')

@section('title')
Home
@endsection

@section('content')
    <div class="content">
        <h1>Selamat Datang!</h1>
        <br>
        <h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem, officiis, incidunt error quo sed deleniti temporibus reprehenderit magnam, vel eveniet nemo doloribus repellat expedita quod at ducimus et odio labore! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Temporibus impedit, libero ipsam aspernatur adipisci earum eum quasi laborum cum sapiente quam iusto praesentium modi dignissimos optio quae eveniet, iure rerum.</p>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Table Admin</h5>
                <p class="card-text">Anda dapat melihat progress task yang harus diisi admin.</p>
                <a href="/table" class="btn btn-primary">Go to Table</a>
            </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Data Table</h5>
                <p class="card-text">Anda dapat melihat data table.</p>
                <a href="/data-table" class="btn btn-primary">Go to Table Data</a>
            </div>
            </div>
        </div>
    </div>

@endsection

