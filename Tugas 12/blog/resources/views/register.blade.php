<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
        <div class="form">
            <h3>Sign up Form</h3>
            <div class="name">
                <p>First name:</p>
                <input type="text" name="firstName">
                <p>Last name:</p>
                <input type="text" name="secondName">
            </div>
            <div class="gender">
                <p>Gender:</p>
                <form>
                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label><br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label><br>
                    <input type="radio" id="other" name="gender" value="other">
                    <label for="other">Other</label>
                </form> 
            </div>
            <div class="nationality">
                <p>Nationality:</p>
                <select name="nationality" id="nationality">
                    <option value="indonesian">Indonesian</option>
                    <option value="Korean">Korean</option>
                    <option value="Japanese">Japanese</option>
                    <option value="Malaysian">Malaysian</option>
                </select>
            </div>
            <div class="language">
                <p>Language Spoken:</p>
                <form>
                    <input type="checkbox" id="language1" name="language1" value="Indoensia">
                    <label for="language1"> Bahasa Indoensia</label><br>
                    <input type="checkbox" id="language2" name="language2" value="English">
                    <label for="language2"> English</label><br>
                    <input type="checkbox" id="language3" name="language3" value="others">
                    <label for="language3"> Others</label>
                </form>
            </div>
            <div class="bio">
                <p>Bio:</p>
                <textarea name="message" rows="10" cols="30"></textarea>
            </div>
                @csrf
                <button type="submit">Sign Up</button>
        </div>
    </form>
    
</body>
</html>