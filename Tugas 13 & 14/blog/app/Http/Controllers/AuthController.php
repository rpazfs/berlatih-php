<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request){
        $namaDepan = $request["firstName"];
        $namaBelakang = $request["secondName"];
        return view('welcome', compact('namaDepan','namaBelakang'));
    }

}